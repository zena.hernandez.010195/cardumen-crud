const db = {
    routes: [
        {
            "data": {
                "time": 60,
                "start": {
                    "location": {	
                        "_latitude": 19.380678,
                        "_longitude": -99.152850
                    },
                    "name": "Dominos Vertiz"
                },
                "finish": {
                    "name": "IBM Santa Fe",
                    "location": {
                        "_latitude": 19.372501,
                        "_longitude": -99.261174
                    }
                },
                "price": 50
            },
            "createdAt": "2019-11-15T05:43:46.498Z"
        }
    ]
}