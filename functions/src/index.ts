import * as functions from 'firebase-functions';
import axios from 'axios';
import * as cors from 'cors';


const admin =  require('firebase-admin');

admin.initializeApp();
const express = require('express');
const app = express();


// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// Levenshtein function
import levenshtein = require('js-levenshtein');

   app.get('/routes', async (req,res) => {
      return res.json(await getAllRoutes());
   });

   app.post('/route', (req,res) => {
      const newRoute = {
         data: req.body.data,
         createdAt: new Date().toISOString()
      };
      admin.firestore()
      .collection('routes')
      .add(newRoute)
      .then(doc => {
         return res.json({message: `Route ${doc.id} created sucessfully`});
      }) 
      .catch(err => {
         res.status(500).json({error: 'something went wrong'});
         console.error(err);
      });
   });

   app.post('/routes/search', async (req,res) => {
      const body = {
         address1: req.body.start.address,
         address2: req.body.finish.address,
         location: req.body.start.location
      };
      console.log('Fetching all existing routes...');
      const allRoutes = await getAllRoutes();
      console.log('Available routes: ');
      allRoutes.forEach(route => console.log(`${route.data.start.name} ----- ${route.data.finish.name}`));
      const maxDistance = 1000;
      const leven_threshold = 3.5
      if (body.address1.length>2 && !body.location && body.address2.length<2) {
         console.log('only START address given is valid...');
         const address = body.address1;
         let locationData: any;
         try{
         const geoData = await getGeocode(address);
         locationData = geoData.data.results[0].geometry.location
         } catch(error){
            console.log('Response status 400 Input address not valid');
            return res.status(400).send('Input address not valid!');
         }
         console.log('Finding routes by name using the START address entered...');
         const matchedRoutesByName = allRoutes.filter(route =>
            route.data.start.name.toUpperCase().includes(address.toUpperCase()) ||
            levenshtein(route.data.start.name.toUpperCase(),address.toUpperCase())<leven_threshold
         );
         console.log(`Found ${matchedRoutesByName.length} routes matched by name`);

         const matchedRoutesByLocation= allRoutes.filter(route =>
             calculateDistance(locationData,route.data.start.location) < maxDistance
         );
         return res.json(matchedRoutesByLocation.concat(matchedRoutesByName.filter((item) => matchedRoutesByLocation.indexOf(item) < 0)));


      } else if(body.address2.length>2 && !body.location && body.address1.length<2) { 
         console.log('only FINISH address given is valid...');
         const address = body.address2;
         let locationData: any;
         try{
         const geoData = await getGeocode(address);
         locationData = geoData.data.results[0].geometry.location
         } catch(error){
            console.log('Response status 400 Input address not valid');
            return res.status(400).send('Input address not valid!');
         }
         console.log('Finding routes by name using the FINISH address entered...');
         const matchedRoutesByName = allRoutes.filter(route =>
            route.data.finish.name.toUpperCase().includes(address.toUpperCase()) ||
            levenshtein(route.data.finish.name.toUpperCase(),address.toUpperCase()) < leven_threshold
         );
         console.log(`Found ${matchedRoutesByName.length} routes matched by name`);
         
         const matchedRoutesByLocation= allRoutes.filter(route =>
             calculateDistance(locationData,route.data.finish.location) < maxDistance
         );

         return res.json(matchedRoutesByLocation.concat(matchedRoutesByName.filter((item) => matchedRoutesByLocation.indexOf(item) < 0)));


      } else if(body.location && !body.address2 && !body.address1) {
         return res.json(allRoutes.filter(route => 
            calculateDistance(body.location,route.data.start.location) < maxDistance));

      } else if(body.address1.length>2 && body.address2.length>2 && !body.location){
         console.log('Given START and FINISH addresses valid...');
         //match routes by name with address1
         console.log('Finding routes by name using the START address entered...');
         const matchedRoutesByNameA1 = allRoutes.filter(route =>
            route.data.start.name.toUpperCase().includes(body.address1.toUpperCase()) ||
            levenshtein(route.data.start.name.toUpperCase(),body.address1.toUpperCase())<leven_threshold
            );
         console.log(`Found ${matchedRoutesByNameA1.length} routes matched by name using START address`);
         
         //match routes by name with address2
         console.log('Finding routes by name using the FINISH address entered...');
         const matchedRoutesByNameA2 = allRoutes.filter(route =>
            route.data.finish.name.toUpperCase().includes(body.address2.toUpperCase()) ||
            levenshtein(route.data.finish.name.toUpperCase(),body.address2.toUpperCase())<leven_threshold);
         console.log(`Found ${matchedRoutesByNameA2.length} routes matched by name using FINISH address`);
         
         //concat routes found by name address1 and address2
         const matchedRoutesByName = matchedRoutesByNameA1.concat(matchedRoutesByNameA2.filter((item) => matchedRoutesByNameA1.indexOf(item) < 0));
         console.log(`Found in total ${matchedRoutesByName.length} routes matched by name using addresses`);
         console.log('Matching routes: ');
         matchedRoutesByName.forEach(route => console.log(`${route.data.start.name} ----- ${route.data.finish.name}`));

         //calculate location of both addresses
         let locationDataStart: any;
         let locationDataFinish: any;
         try{
            const geoDataA1 = await getGeocode(body.address1);
            locationDataStart = geoDataA1.data.results[0].geometry.location;
            const geoDataA2 = await getGeocode(body.address2);
            locationDataFinish = geoDataA2.data.results[0].geometry.location;
         }catch (error){
            console.log('Response status 400 Input address not valid');
            return res.status(400).send('Input address not valid!');
         }

         //calculate distance of both addresses to routes and return the matching array
         const matchedRoutesByLocation = allRoutes.filter(route =>
            calculateDistance(locationDataStart, route.data.start.location) < maxDistance ||
            calculateDistance(locationDataFinish, route.data.finish.location) < maxDistance);
      
            return res.json(matchedRoutesByLocation.concat(matchedRoutesByName.filter((item) => matchedRoutesByLocation.indexOf(item) < 0)));
      
      } else if(body.address1.length<2 && body.address2.length<2 && !body.location){     
         return res.status(400).send('No matching routes were found!');
      } else {
         return res.status(400).send('No matching routes were found!');
      }
   });

   const getGeocode = async (address: String): Promise<any> => {
      console.log(`Entering to fetch geocode of address: ${address}...`)
      const apiKey = 'AIzaSyCU9d0mNuvKMmzANhH2xxJArhojFrtv-7g';
      const options = {
         url: `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${apiKey}`,
         json: true
      };
      try {
         return await axios.get(options.url);
      } catch (error) {
         console.log('Could not get GEOCODE')
         return [];
      }
   }

   const getAllRoutes = async () => {
      console.log('Entering to get all av routes...');
      try {
      const data = await admin.firestore().collection('routes').get()
      const routes = [];
      data.forEach(doc => {
            routes.push({
               routeId: doc.id,
               ...doc.data()
            });
      })
      return routes;
      } catch (err) {
         return [];
      }
      }

   const calculateDistance = (location1, location2) => {
      console.log('Entering to calculate distances');
      const lat1: number = location1.lat;
      const lon1: number = location1.lng;
      const lat2 = location2._latitude;
      const lon2 = location2._longitude;
      console.log('Latitud point 1 = '+lat1);
      console.log('Longitude 1 = '+lon1);
      console.log('Latitud point 2 = '+lat2);
      console.log('Longitude point 2 = '+lon2);

      const R = 6371e3; // metres
      const φ1 = toRad(lat1);
      const φ2 = toRad(lat2);
      const Δφ = toRad((lat2-lat1));
      const Δλ = toRad((lon2-lon1));
         
      const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ/2) * Math.sin(Δλ/2);

      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
         
      const d = R * c;
      console.log('distance= '+ d)
      console.log('\n');
      return d;
   }

   /** Converts numeric degrees to radians */
   function toRad(Value) {
      return Value * Math.PI / 180;
  }


exports.api = functions.https.onRequest(app);